import React from 'react';
import PropTypes from 'prop-types';

import './Quote.scss';

/**
 * Quote
 * @description [Description]
 * @example
  <div id="Quote"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Quote, {
        title : 'Example Quote'
    }), document.getElementById("Quote"));
  </script>
 */
class Quote extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'quote';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				Quote
			</div>
		);
	}
}

Quote.defaultProps = {
	children: null
};

Quote.propTypes = {
	children: PropTypes.node
};

export default Quote;
